from __future__ import annotations
import random


class Range:
    def __init__(self, start: float, end: float, step: float):
        self.start = start
        self.end = end
        self.step = step
        self.now = start

    def __iter__(self) -> Range:
        return self

    def __next__(self) -> float:
        if self.now > self.end:
            raise StopIteration()
        else:
            now = self.now
            self.now += self.step
            return now


def random_gen():
    while True:
        i = random.randint(1, 100)
        if i % 27 != 0:
            yield i
        else:
            break
