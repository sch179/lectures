# Материалы к лекциям по программированию

[[_TOC_]]

## 2024-03-02
Введение в асинхронное программирование. [Тык](2024-03-02)

## 2024-03-30
Начала геометрии. [Тык](2024-03-30)

## 2024-04-06
Замены координат. [Тык](2024-04-06)

## 2024-04-13
ООП и геометрия. Коники. [Тык](2024-04-13)

## 2024-04-20
Генераторы и итераторы. [Тык](2024-04-20)

## 2024-05-04
Json и сервер. [Тык](2024-05-04)
