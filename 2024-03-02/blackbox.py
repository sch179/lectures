import asyncio
import random


async def black_box(q: asyncio.Queue[str], am: int) -> None:
    for i in range(am):
        await q.put('a' * random.randint(2, 4))
        print('putted')
        await asyncio.sleep(random.randint(1, 2) * 0.25)
    await q.join()


async def worker(q: asyncio.Queue[str], am: int) -> int:
    result = 0
    while True:
        new = await q.get()
        print(f'{new} getted by {am}')
        result += len(new)
    return result


async def main() -> None:
    q: asyncio.Queue[str] = asyncio.Queue()
    blackbox = asyncio.create_task(black_box(q, 30))
    tasks = []
    for i in range(1, 20):
        tasks.append(asyncio.create_task(worker(q, i)))
    print('joined')
    print(sum(await asyncio.gather(*tasks)))



asyncio.run(main())
