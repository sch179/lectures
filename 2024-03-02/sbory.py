import asyncio
import random
import base64
import time

TASKS = {}


async def email(teacher: int) -> str:
    print(f'{teacher} received')
    await asyncio.sleep(teacher + random.randint(-2, 2))
    task = str(teacher) + ' ' + str(base64.b64encode(random.randbytes(16)))
    print(f'{teacher} generated task {task}')
    TASKS[teacher // 2] = task
    return task


async def main() -> None:
    ts = [i for i in range(15)]
    start = time.monotonic()
    async with asyncio.TaskGroup() as tg:
        for t in ts:
            tg.create_task(email(t))
    end = time.monotonic()
    print(f'{end - start} second passed')
    from pprint import pprint
    pprint(TASKS)


if __name__ == '__main__':
    asyncio.run(main())
