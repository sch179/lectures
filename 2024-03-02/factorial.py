import sys
import asyncio


def sync_factorial(n: int) -> int:
    res = int(n)
    for i in range(1, n):
        res *= i
    return res


async def worker(idx: int, q: asyncio.Queue[int], d: dict[int, int]) -> None:
    am = 0
    d[idx] = 1
    while not q.empty():  # пока ещё остались числа ...
        a = await q.get()  # возьмём одно ...
        d[idx] *= a  # и домножим на наш текущий ответ
        am += 1
        q.task_done()  # отметитмся, что мы обработали a
        if word == 'sleep':
            await asyncio.sleep(0)
    print(f'{idx} обработал {am} чисел')


async def factorial(n: int, w: int) -> int:
    """
    Будем считать фактириал асинхронно.
    Каждая task -- "работник", который берёт из общего места (очереди) число и множит на своё
    """
    q: asyncio.Queue[int] = asyncio.Queue()  # очередь с числами
    d: dict[int, int] = {}  # словарь для ответов
    for i in range(1, n + 1):
        q.put_nowait(i)  # наполнили очередь числами
    tasks = []
    for i in range(w):
        task = asyncio.create_task(worker(i, q, d))  # nota bene: в этот момент стартуют работники!
        tasks.append(task)
    await q.join()  # ждём исчерпания очереди
    result = 1
    for v in d.values():
        result *= v
    return result


word = input()
n, w = map(int, input().split())
sys.set_int_max_str_digits(1000000000)
if word == 'sync':
    print(sync_factorial(n))
else:
    print(asyncio.run(factorial(n, w)))
