from math import atan2
import asyncio
import random


async def f(taskname: str) -> None:
    print(taskname)
    await asyncio.sleep(random.randint(1, 4))
    p = map(float, input().split())
    print(atan2(*p))


async def s(taskname: str) -> None:
    print(taskname)
    await asyncio.sleep(random.randint(1, 4))
    coro = f('third')
    await coro
    a = input()
    b = a.split()
    for w in b:
        print(w)


async def main() -> None:
    async with asyncio.TaskGroup() as tg:
        tg.create_task(f('first'))
        tg.create_task(s('second'))


if __name__ == '__main__':
    asyncio.run(main())
