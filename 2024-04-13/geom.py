from typing import TypeVar


class Matrix:
    pass


class Point(Matrix):
    pass


T = TypeVar('T', bound=Matrix)


class Transformation(Matrix):
    def do(self, other: T) -> T:
        match other:
            case Point():
                return self @ other
            case Transformation():
                return self @ other  # other @ self ???
            case Conic():
                inv = self.inverse()
                return inv.transpose() @ other @ inv


class Shift(Transformation):
    pass


class Conic(Matrix):
    def __init__(self, x: float, y: float):
        super().__init__()
        self.x = x
        self.y = y


class Ellipse(Conic):  # только оружность
    def __init__(self, x: float, y: float, r: float):
        super().__init__(x, y)
        self.r = r
