from __future__ import annotations
import math
from dataclasses import dataclass
from typing import Any


@dataclass
class Vector:
    x: float
    y: float

    def length(self) -> float:
        return math.hypot(self.x, self.y)


@dataclass
class Point:
    x: float
    y: float

    def __sub__(self, other: Any) -> Vector:
        match other:
            case Point():
                return Vector(self.x - other.x, self.y - other.y)
            case _:
                return NotImplemented

    @staticmethod
    def dist(self: Point, other: Point) -> float:
        return (self - other).length()


if __name__ == '__main__':
    A = Point(*map(int, input().split()))
    B = Point(*map(int, input().split()))
    print(Point.dist(A, B))
