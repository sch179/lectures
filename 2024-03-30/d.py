from __future__ import annotations
from dataclasses import dataclass
from typing import ClassVar


@dataclass
class Domino:
    name: str
    surname: str

    term: ClassVar[int] = 10
    letter: ClassVar[str] = 'D'
    eyes_color: str = ""
    height: int = 0
    weight: int = 0
    vseroses: int = 0
