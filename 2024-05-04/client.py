import asyncio
import json
import aiohttp


async def main() -> None:
    async with aiohttp.ClientSession() as session:
        params = {'data': json.dumps([{'width': 2, 'height': 2, 'elems': [1, 2, 3, 4]},
                                      {'width': 2, 'height': 2, 'elems': [-1.9999999999999996, 0.9999999999999998, 1.4999999999999998, -0.4999999999999999]}])}
        async with session.get('http://51.250.112.20:8179/matrix/prod', params=params) as resp:
            print(resp.status)
            print(await resp.text())
            data = await resp.json()
            print(data)


asyncio.run(main())
