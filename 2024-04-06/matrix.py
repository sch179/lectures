from __future__ import annotations
import math
from dataclasses import dataclass
from typing import Any


@dataclass
class Vector:
    x: float
    y: float

    def __matmul__(self, other: Any) -> float:
        if not isinstance(other, Vector):
            return NotImplemented
        return self.x * other.x + self.y * other.y


@dataclass
class Matrix2D:
    top: Vector
    bot: Vector

    @classmethod
    def NewCoord(cls, e1: Vector, e2: Vector) -> Matrix2D:
        return Matrix2D(Vector(e1.x, e2.x),
                        Vector(e1.y, e2.y))

    def trans(self) -> Matrix2D:
        return Matrix2D(Vector(self.top.x, self.bot.x),
                        Vector(self.top.y, self.bot.y))

    def __matmul__(self, other: Any) -> Vector | Matrix2D:
        match other:
            case Vector():
                return Vector(self.top @ other, self.bot @ other)
            case Matrix2D():
                other = other.trans()
                ntop = self @ other.top
                nbot = self @ other.bot
                return Matrix2D(ntop, nbot).trans()
            case _:
                return NotImplemented
