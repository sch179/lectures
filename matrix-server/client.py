import json
import aiohttp
import asyncio


async def main() -> None:
    async with aiohttp.ClientSession() as session:
        params = {'data': json.dumps([{'width': 3, 'height': 2, 'elems': [1, 2, 3, 4, 5, 6]}])}
        async with session.get('http://51.250.112.20:8080/matrix/echo', params=params) as resp:
            print(resp.status)
            print(await resp.text())


asyncio.run(main())
