import json
import asyncio
from typing import Any
import numpy
from aiohttp import web


routes = web.RouteTableDef()


@routes.get('/health')
async def health(request: web.Request) -> web.Response:
    return web.Response(text='Ok')


@routes.get('/matrix/{method}')
async def server(request: web.Request) -> web.Response:
    req_data = json.loads(request.query.get('data', ''))
    matrices = []
    for m in req_data:
        w = m.get('width', 0)
        h = m.get('height', 0)
        elems = m.get('elems', [])
        mat = []
        for j in range(h):
            r = []
            for i in range(w):
                r.append(float(elems[j * w + i]))
            mat.append(r)
        matrices.append(numpy.array(mat))
    match request.match_info['method']:
        case 'echo':
            result = matrices[0]
        case 'sum':
            result = matrices[0] + matrices[1]
        case 'prod':
            result = matrices[0] @ matrices[1]
        case 'transpose':
            result = numpy.transpose(matrices[0])
        case 'inverse':
            result = numpy.linalg.inv(matrices[0])
        case '_':
            raise web.HTTPBadRequest
    h, w = result.shape
    elems = []
    for j in range(h):
        for i in range(w):
            elems.append(result[j, i])
    data: dict[str, Any] = {'method': request.match_info['method'], 'result': {'width': w, 'height': h, 'elems': elems}}
    await asyncio.sleep(5)
    return web.json_response(data)

app = web.Application()
app.add_routes(routes)
web.run_app(app, port=8179)
